<?php
/**
 * Created by PhpStorm.
 * User: DOM
 * Date: 11.04.2019
 * Time: 19:14
 */

namespace App\Infrastructure\Repository;

use App\PdoMysql;

class UpdateSheetsRepository
{
    private $pdo;

    public function __construct( PdoMysql $pdo)
    {
        $this->pdo = $pdo->pdo;
    }

    public function setUpdateHistory($fileName, $dataContent){
        $query = $this->pdo->prepare('
            INSERT INTO `updates_history_files`(`file_name`, `data_content`) 
              VALUES (:fileName,:dataContent);
            INSERT INTO `updates_history`(`file_id_fk`, `create_time`)
              VALUES (LAST_INSERT_ID(), NOW());
        ');
        $query -> bindParam(':fileName', $fileName);
        $query -> bindParam(':dataContent', $dataContent);
        $query -> execute();
    }
}