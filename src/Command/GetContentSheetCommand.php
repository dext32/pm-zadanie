<?php

namespace App\Command;

use App\QuickstartGoogleApi;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetContentSheetCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('pm:display-sheet')
        ;


        // @todo required inline parameters
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $google_api = new QuickstartGoogleApi();

        $output->writeln($google_api->getSheet());

        // @todo init
    }

}