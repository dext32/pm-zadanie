<?php

namespace App\Command;

use App\Infrastructure\Repository\UpdateSheetsRepository;
use App\QuickstartGoogleApi;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UpdateSheetCommand extends Command
{

    private $container;

    public function __construct($name = null, ContainerInterface $container)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName('pm:update-sheet')
            ;


        // @todo required inline parameters
    }


    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $google_api = new QuickstartGoogleApi();

        //$output->writeln($google_api->updateSheet());

        $repo = $this->container->get('app.update_sheets_repository');
        printf($repo->setUpdateHistory('data.json', '["0":["1","gasoline"],"1":["2","bin"]]'));

        // @todo init
    }

}