<?php

namespace App;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Google_Service_Sheets_ClearValuesRequest;

class QuickstartGoogleApi
{
    private static $spreadsheetId = '1XdP-MAnzyUO1cI7dLNCx3PbG0h119VM6zcEbpvnSLQs';
    private static $range = 'arkusz';

    function getClient() {
        $client = new Google_Client();
        $client->setAuthConfig(__DIR__.'\..\pmweb-5aa3d9098ad8.json');
        $client->setApplicationName('zadanie json data');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
        return $client;
    }

    public function getSheet()
    {
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        $response = $service->spreadsheets_values->get(self::$spreadsheetId, self::$range);
        $values     = $response->getValues();
        $numRows    = $response->getValues() != null ? count($response->getValues()) : 0;

        if (empty($values)) {
            print "No data found.\n";
        } else {
            printf("%d rows retrieved.", $numRows);
            printf(var_export($response, true));
        }

    }

    public function clearSheet()
    {
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);
        $requestBody = new Google_Service_Sheets_ClearValuesRequest();

        $response = $service->spreadsheets_values->clear(self::$spreadsheetId, self::$range, $requestBody);
        return $response ? var_export($response, true) : print 'Cannot clear sheet';
    }

    public function updateSheet()
    {
        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Sheets($client);

        $json_file = file_get_contents(__DIR__.'\..\data.json');
        $json_data = json_decode($json_file);

        $values = [];
        foreach ($json_data->data as $row){
            array_push($values,$row);
        }

        $body = new Google_Service_Sheets_ValueRange([
            'values' => $values
        ]);

        $params = [
            'valueInputOption' => ['USER_ENTERED']
        ];

        $this->clearSheet();
        $result = $service->spreadsheets_values->update(self::$spreadsheetId, self::$range, $body, $params);
        printf("%d cells updated.", $result->getUpdatedCells());
    }

}